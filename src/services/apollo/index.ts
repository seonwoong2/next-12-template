import { ApolloClient, from, HttpLink, InMemoryCache, NormalizedCacheObject } from '@apollo/client'
import { setContext } from '@apollo/client/link/context'
import { onError } from '@apollo/client/link/error'
import { isBrowser } from 'constants/common'
import { getStorageData, setStorageData } from 'utils/common'
import { ECookie } from 'types/util'
import { makeHMACSignature } from 'utils/hmac'
import { IErrors } from 'types/service'
import { getErrorData } from 'utils/error'
import { showErrorPopup } from 'utils/customEvent'

let apolloClient: ApolloClient<NormalizedCacheObject>
const host = process.env.HOST

const createApolloClient = (lang, token, extraHeaders) => {
  const dabiGamesAuthLink = setContext((_, { headers }) => {
    // return the headers to the context so httpLink can read them
    const timestamp = new Date().getTime()
    const signature = makeHMACSignature('POST', '/graphql', timestamp)
    if (isBrowser) {
      if (extraHeaders.host !== undefined) {
        setStorageData(ECookie.HOST, extraHeaders.host)
      }
      token = getStorageData(ECookie.ACCESS_TOKEN)
      lang = lang ?? getStorageData(ECookie.LANGUAGE)
    }
    return {
      headers: {
        ...extraHeaders,
        ...headers,
        'x-w-api-signature': signature,
        'x-w-api-timestamp': timestamp,
        'x-w-user-agent': 'finent.io',
        'x-w-app-id': 'gamebet',
        authorization: token ? `Bearer ${token}` : '',
        'x-language': lang,
        'x-w-domain': 'dev.dabi-games.dabi.app',
      },
    }
  })

  const errorLink = onError(({ graphQLErrors, networkError }) => {
    if (graphQLErrors) {
      graphQLErrors.forEach(({ message, locations, path, ...props }) => {
        const error: IErrors = { ...props, message }
        const { id, defaultMessage, buttonType } = getErrorData(error.code)
        showErrorPopup({ buttonType: buttonType, title: '', message, ...props })
        console.info(`[G error]: Message: ${message}, Location: ${locations}, Path: ${path}`)
      })
    }
    if (networkError) console.info(`[Networkerror]: ${networkError}`)
  })

  // usage: useQuery({})
  const dabiLink = dabiGamesAuthLink.concat(
    new HttpLink({
      uri: !isBrowser ? `${host}/dabi-games` : `/dabi-games`,
      credentials: 'same-origin',
    }),
  )
  const splitLink = dabiLink

  return new ApolloClient({
    ssrMode: !isBrowser,
    link: from([errorLink, splitLink]),
    defaultOptions: {
      watchQuery: {
        fetchPolicy: 'no-cache',
      },
      query: {
        fetchPolicy: 'no-cache',
      },
      mutate: {
        fetchPolicy: 'no-cache',
      },
    },
    cache: new InMemoryCache({ dataIdFromObject: () => null }),
    connectToDevTools: process.env.NODE_ENV !== 'production',
  })
}

export const initializeApollo = ({ initialState = null, lang = 'en', token = '', headers = {} }: any) => {
  const _apolloClient = apolloClient ?? createApolloClient(lang, token, headers)

  if (initialState) _apolloClient.cache.restore(initialState)

  if (!isBrowser) return _apolloClient

  if (!apolloClient) apolloClient = _apolloClient

  return _apolloClient
}
