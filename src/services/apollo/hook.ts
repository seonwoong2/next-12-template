import { useMemo } from 'react'

import { initializeApollo } from '.'

export const useApollo = (initialState: any = null, token = '', lang = '', headers = {}) => {
  const store = useMemo(() => {
    return initializeApollo({ initialState, lang, token, headers })
  }, [headers, initialState, lang, token])

  return store
}
