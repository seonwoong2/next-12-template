import React from 'react'
import { IDefaultProps } from 'constants/common'
import { ERouterPath } from 'types/routing'
import { LanguageButton } from 'components/molecule/LanguageButton'
import { useIntl } from 'react-intl'

import { StyledBasicHeaderBox, StyledBaicHeaderLogoBox, StyledBasicHeaderFunctionBox, StyledBasicHeaderLoginButton } from './BasicHeader.style'

const BasicHeader: React.FC<IDefaultProps> = ({ deviceType }) => {
  const { formatMessage } = useIntl()
  return (
    <StyledBasicHeaderBox>
      <StyledBaicHeaderLogoBox>LOGO</StyledBaicHeaderLogoBox>
      <StyledBasicHeaderFunctionBox>
        <StyledBasicHeaderLoginButton href={ERouterPath.LOGIN}>{formatMessage({ id: 'login' })}</StyledBasicHeaderLoginButton>
        <LanguageButton />
      </StyledBasicHeaderFunctionBox>
    </StyledBasicHeaderBox>
  )
}

export default BasicHeader
