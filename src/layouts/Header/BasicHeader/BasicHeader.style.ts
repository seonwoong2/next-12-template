import styled from 'styled-components'
import { Button, A } from 'styles/common.style'

export const StyledBasicHeaderBox = styled.div`
  display: flex;
  height: 72px;
  border-bottom: 1px solid lightgray;
  padding: 0 40px;

  //임시
  justify-content: space-between;
  align-items: center;
`

export const StyledBaicHeaderLogoBox = styled.div`
  display: flex;
  flex: 1;
  height: 100%;
  align-items: center;
`

export const StyledBasicHeaderFunctionBox = styled.div`
  display: flex;
  flex: 1;
  height: 100%;
  align-items: center;
  justify-content: end;
`

export const StyledBasicHeaderLoginButton = styled(A)`
  padding: 12px 10px;
  border: none;
  background-color: transparent;
  color: rgb(78, 89, 104);
  font-size: ${({ theme }) => theme.font.size.base}px;
  text-decoration: none;
  cursor: pointer;
  border-radius: ${({ theme }) => theme.border.radius.ms};
  &:hover {
    background-color: rgba(2, 32, 71, 0.05);
  }
`
