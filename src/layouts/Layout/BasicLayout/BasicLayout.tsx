import React from 'react'
import { IDefaultProps } from 'constants/common'
import { BasicHeader } from 'layouts/Header/BasicHeader'
import { BasicFooter } from 'layouts/Footer/BasicFooter'
import { useMounted } from 'hooks/useMounted'

import { StyledBasicLayoutBox, StyledbasicLayoutChildrenBox } from './BasicLayout.style'

interface IProps extends IDefaultProps {
  children: React.ReactNode
  header?: React.ReactNode
  footer?: React.ReactNode
  useHeader?: boolean
  useFooter?: boolean
}

const BasicLayout: React.FC<IProps> = ({ deviceType, header = <BasicHeader />, footer = <BasicFooter />, useHeader = true, useFooter = true, children }) => {
  const mounted = useMounted()
  return (
    <>
      {mounted && (
        <StyledBasicLayoutBox>
          {useHeader && header}
          <StyledbasicLayoutChildrenBox>{children}</StyledbasicLayoutChildrenBox>
          {useFooter && footer}
        </StyledBasicLayoutBox>
      )}
    </>
  )
}

export default BasicLayout
