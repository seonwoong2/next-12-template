import React from 'react'
import { IDefaultProps } from 'constants/common'

import { StyledBasicFooterBox } from './BasicFooter.style'

const BasicFooter: React.FC<IDefaultProps> = ({ deviceType }) => {
  return <StyledBasicFooterBox>FOOTER</StyledBasicFooterBox>
}

export default BasicFooter
