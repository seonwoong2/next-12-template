import 'styled-components'

import { ThemeBaseColorTypes, ThemeBorderTypes, ThemeFontTypes } from '.'

declare module 'styled-components' {
  export interface DefaultTheme {
    baseColor: ThemeBaseColorTypes
    font: ThemeFontTypes
    border: ThemeBorderTypes
  }
}
