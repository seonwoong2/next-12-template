import { DefaultTheme } from 'styled-components'

const font = {
  size: {
    xs: 12,
    sm: 13,
    sl: 14,
    base: 15,
    ms: 16,
    md: 18,
    lg: 21,
    xl: 24,
    '2xl': 28,
    '3xl': 36,
    '4xl': 40,
    '5xl': 45,
    '6xl': 55,
    '7xl': 65,
    '8xl': 79,
    '9xl': 88,
    '10xl': 99,
  },
  weight: {
    body: 400,
    heading: 700,
    thin: 100,
    light: 300,
    regular: 400,
    medium: 500,
    bold: 700,
    bolder: 900,
    large: 1000,
  },
}

const baseColor = {
  white: '#ffffff',
  gray: {
    100: '#f9f9f9',
    200: '#F7F7F7',
    300: '#f4f4f4',
    400: '#F3F3F3',
    500: '#f1f1f1',
    600: '#EdEdEd',
    700: '#E6E6E6',
    800: '#C2C3CC',
    900: '#bdbdbd',
    1000: '#AEAEAE',
    1100: '#929292',
    1200: '#717171',
  },
  black: '#000',
  red: '#FF0000',
}

const border = {
  radius: {
    sm: '3px',
    base: '6px',
    ms: '8px',
    md: '12px',
    lg: '18px',
  },
}

export const theme: DefaultTheme = {
  baseColor,
  font,
  border,
}

export type ThemeBaseColorTypes = typeof baseColor
export type ThemeFontTypes = typeof font
export type ThemeBorderTypes = typeof border
