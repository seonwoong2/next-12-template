import { TypedUseSelectorHook, useSelector } from 'react-redux'
import { RootState } from 'stores/configure'

export const useReduxSelector: TypedUseSelectorHook<RootState> = useSelector
