import { D_MIN, M_MAX, T_MAX, T_MIN } from "constants/common";
import { useEffect, useState } from "react";
const isClient = typeof window === "object";

export const useMedia = (query: string) => {
  const [state, setState] = useState(
    isClient ? () => window.matchMedia(query).matches : false
  );

  useEffect(() => {
    let mounted = true;
    const mql = window.matchMedia(query);
    mql.onchange = () => {
      if (!mounted) {
        return;
      }
      setState(!!mql.matches);
    };
    setState(mql.matches);
    return () => {
      mounted = false;
      mql.removeEventListener("change", () => {});
    };
  }, [query]);

  return state;
};

export const useGetDeviceType = () => {
  return {
    mobile: useMedia(`(max-width: ${M_MAX}px)`),
    tablet: useMedia(`(min-width: ${T_MIN}px) and (max-width: ${T_MAX}px)`),
    desktop: useMedia(`(min-width: ${D_MIN}px)`),
  };
};
