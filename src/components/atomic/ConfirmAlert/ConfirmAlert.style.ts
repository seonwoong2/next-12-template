import styled from 'styled-components'
import React from 'react'
import { Text } from 'styles/common.style'

export const ConfirmAlertContainer = styled.div<{ style: React.CSSProperties }>`
  @keyframes fadeIn {
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }
  display: flex;
  flex-direction: column;
  padding: 10px;
  height: auto;
  // width: 250px;
  width: ${({ style }) => (style?.width ? `${style.width}px` : '250px')};
  z-index: 10000000;
  background-color: gray;
  border-radius: 10px;
  color: white;
  border: gray;
  animation: fadeIn 0.3s;
`

export const ConfirmTitleWrapper = styled.div`
  display: block;
  line-height: 2;
  flex: 0;
  align-self: center;
`

export const ConfirmTitle = styled(Text)`
  font-size: 18;
  font-weight: bold;
`

export const ConfirmMessageWrapper = styled.div`
  display: flex;
  justify-content: center;
  flex: 1;
  margin: 10px;
  align-items: center;
`
export const ConfirmMessage = styled(Text)`
  overflow-wrap: anywhere;
  word-break: keep-all;
`

export const ConfirmButtonWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  flex: 0;
  height: 100%;
  padding-top: 10px;
`
export const CancelButton = styled.button`
  width: 45%;
  height: 40px;
  border: 1px solid transparent;
  border-radius: 5px;
  color: white;
  background: red;
  mix-blend-mode: screen;
  cursor: pointer;
`
export const ConfirmButton = styled.button`
  width: 45%;
  height: 40px;
  background-color: blue;
  color: white;
  border-radius: 5px;
  border-width: 0;
  cursor: pointer;
  &.home {
    width: 100%;
  }
`
