import React from 'react'
import { AlertButtonType } from 'types/util'

import {
  ConfirmAlertContainer,
  ConfirmTitle,
  ConfirmTitleWrapper,
  ConfirmMessage,
  ConfirmMessageWrapper,
  ConfirmButton,
  ConfirmButtonWrapper,
  CancelButton,
} from './ConfirmAlert.style'

interface IProps {
  title?: string
  message?: string
  style?: any
  onConfirm?: any
  onCancel?: any
  onClose?: any
  buttonType?: AlertButtonType
}

const ConfirmAlert: React.FC<IProps> = ({
  title = '',
  message = '',
  style,
  buttonType = 'default',
  onCancel = () => {},
  onClose = () => {},
  onConfirm = () => {},
}) => {
  return (
    <ConfirmAlertContainer style={style}>
      <ConfirmTitleWrapper>
        <ConfirmTitle>{title}</ConfirmTitle>
      </ConfirmTitleWrapper>
      <ConfirmMessageWrapper>
        <ConfirmMessage>{message}</ConfirmMessage>
      </ConfirmMessageWrapper>
      {buttonType === 'default' && (
        <ConfirmButtonWrapper>
          <CancelButton type="button" onClick={onCancel}>
            취소
          </CancelButton>
          <ConfirmButton type="button" onClick={onConfirm}>
            확인
          </ConfirmButton>
        </ConfirmButtonWrapper>
      )}
    </ConfirmAlertContainer>
  )
}

export default ConfirmAlert
