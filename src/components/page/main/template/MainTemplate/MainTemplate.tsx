import React from 'react'
import { IDefaultProps, isBrowser } from 'constants/common'
import { useAppDispatch } from 'contexts/app/AppProvider'
import { ECustomEvent } from 'types/util'

const MainTemplate: React.FC<IDefaultProps> = () => {
  const dispatch = useAppDispatch()

  const closeDialog = () => {
    dispatch({ type: 'RESET' })
  }

  const test = () => {
    if (isBrowser) {
      window.dispatchEvent(
        new CustomEvent(ECustomEvent.ERROR_POPUP, {
          detail: {
            title: '타이틀',
            message: '메시지',
            needTranslate: false,
            forcePopup: false,
            buttonType: 'default',
            onConfirm: () => closeDialog(),
            onclose: () => closeDialog(),
            onCancel: () => closeDialog(),
            code: 205,
          },
        }),
      )

      dispatch({ type: 'ERROR', payload: { code: 205, message: 'test' } })
    }
  }
  return (
    <div>
      <h1>MAIN</h1>
      <button onClick={() => test()}>test</button>
    </div>
  )
}

export default MainTemplate
