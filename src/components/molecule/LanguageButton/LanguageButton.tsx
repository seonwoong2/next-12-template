import React, { useEffect, useState } from 'react'
import { ELanguage, LANGUAGES } from 'types/common'
import { getStorageData, setStorageData } from 'utils/common'
import { ECookie } from 'types/util'
import EarthIcon from 'assets/icons/EarthIcon'
import { useRouter } from 'next/router'
import find from 'lodash/find'

import { StyledLanguageButtonBox, StyledLanguageText, StyledSelectBox, StyledLanguageButton, StyledSelectLanguage } from './LanguageButton.style'

interface IProps {}

const LanguageButton: React.FC<IProps> = () => {
  const router = useRouter()
  const curLang = getStorageData(ECookie.LANGUAGE) || ELanguage.EN
  const [open, setOpen] = useState<boolean>(false)
  const selectedLanguage = find(LANGUAGES, (x) => x.id === curLang)

  const handleSetLanguage = async (e, lang) => {
    setOpen(false)
    if (curLang !== lang) {
      setStorageData(ECookie.LANGUAGE, lang)
      router.reload()
    }
  }

  return (
    <StyledLanguageButtonBox>
      <StyledLanguageButton onClick={() => setOpen((prev) => !prev)}>
        <EarthIcon />
        <StyledLanguageText>{selectedLanguage?.defaultMessage || 'English'}</StyledLanguageText>
      </StyledLanguageButton>
      <StyledSelectBox open={open}>
        {LANGUAGES?.map((el, idx) => {
          return (
            <StyledSelectLanguage key={idx} onClick={(e) => handleSetLanguage(e, el.id)}>
              {el.defaultMessage}
            </StyledSelectLanguage>
          )
        })}
      </StyledSelectBox>
    </StyledLanguageButtonBox>
  )
}

export default LanguageButton
