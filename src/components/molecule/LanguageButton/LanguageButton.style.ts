import styled from 'styled-components'
import { Button, Text } from 'styles/common.style'

export const StyledLanguageButtonBox = styled.div`
  position: relative;
  margin-left: 20px;
`

export const StyledLanguageButton = styled.div`
  background-color: transparent;
  border-radius: ${({ theme }) => theme.border.radius.ms};
  cursor: pointer;
  padding: 12px 10px;
  display: flex;
  align-items: center;
  &:hover {
    background-color: rgba(2, 32, 71, 0.05);
  }
`

export const StyledLanguageText = styled(Text)`
  color: rgb(78, 89, 104);
  margin-left: 8px;
`

export const StyledSelectBox = styled.div<{ open: boolean }>`
  position: absolute;
  top: 60px;
  display: flex;
  flex-direction: column;
  width: 100%;
  z-index: 99999;
  box-shadow: rgba(0, 0, 0, 0.35) 0px -5px 20px;
  border-radius: ${({ theme }) => theme.border.radius.ms};
  transition: 0.35s all;
  transform: ${({ open }) => (open ? 'scaleY(1)' : 'scaleY(0)')};
  transform-origin: top center;
  background-color: white;

  &::after {
    content: '';
    position: absolute;
    top: 0;
    left: 50%;
    width: 0;
    height: 0;
    border: 15px solid transparent;
    border-bottom-color: white;
    border-top: 0;
    margin-left: -15px;
    margin-top: -15px;
  }
`

export const StyledSelectLanguage = styled(Button)`
  background-color: white;
  border: none;
  padding: 12px 10px;
  cursor: pointer;
  border-radius: ${({ theme }) => theme.border.radius.ms};

  &:hover {
    background-color: rgba(2, 32, 71, 0.05);
  }
`
