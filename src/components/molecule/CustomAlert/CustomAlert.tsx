import React, { useEffect, useState } from 'react'
import { useIntl } from 'react-intl'
import { useRouter } from 'next/router'
import { ECustomEvent, IConfirmAlertArgs } from 'types/util'
import { useAppDispatch } from 'contexts/app/AppProvider'
import { isBrowser } from 'constants/common'

import { Wrapper } from './CustomAlert.style'
import ConfirmAlert from 'components/atomic/ConfirmAlert/ConfirmAlert'

interface IProps {}

const CustomAlert: React.FC<IProps> = () => {
  // const { formatMessage } = useIntl()
  const dispatch = useAppDispatch()
  const router = useRouter()

  const [open, setOpen] = useState<boolean>(false)
  const [options, setOptions] = useState<IConfirmAlertArgs>({
    code: -1,
    status: -1,
    title: '',
    message: '',
    buttonType: 'default',
    style: null,
    onConfirm: () => {},
    onClose: () => {},
    onCancel: () => {},
  })

  if (isBrowser) {
    window.addEventListener(ECustomEvent.ERROR_POPUP, (e: CustomEvent) => {
      let message = e?.detail?.message
      if (e.detail?.needTranslate) {
        // message = formatMessage({ id: e.detail?.message }, { code: e.detail?.code })
      }
      setOptions((prev) => {
        return { ...prev, ...e.detail, message }
      })
    })
  }

  useEffect(() => {
    if (options.forcePopup) {
      setOpen(true)
    } else {
      if (options.code !== -1) {
        setOpen(true)
      }
    }
  }, [dispatch, options])

  return (
    <>
      {open && (
        <div
          style={{
            position: 'fixed',
            left: 0,
            top: 0,
            height: '100%',
            width: '100%',
            zIndex: 99999999999999,
            // height: window.outerHeight,
            // width: window.outerWidth,
          }}>
          <div
            style={{
              // height: window.outerHeight,
              // width: window.outerWidth,
              height: '100%',
              width: '100%',
              position: 'absolute',
              backgroundColor: 'black',
              opacity: 0.5,
            }}
          />
          <Wrapper className="error">
            <ConfirmAlert
              title={options.title ?? ''}
              message={options.message}
              buttonType={options.buttonType ?? 'confirm'}
              style={options.style}
              onClose={() => {
                if (options.onClose) options.onClose()
                setOpen(false)
              }}
              onConfirm={() => {
                if (options.onConfirm) options.onConfirm()
                setOpen(false)
              }}
              onCancel={() => {
                if (options.onCancel) options.onCancel()
                setOpen(false)
              }}
            />
          </Wrapper>
        </div>
      )}
    </>
  )
}

export default CustomAlert
