import { AnyAction, combineReducers, configureStore, EnhancedStore, Store } from '@reduxjs/toolkit'
import { isProduction } from 'constants/common'
import { createWrapper, HYDRATE, MakeStore } from 'next-redux-wrapper'
import { useDispatch } from 'react-redux'

import { IRootReducerState } from './types/common'

import common from './common'

const rootReducer = (state: IRootReducerState | undefined, action: AnyAction) => {
  switch (action.type) {
    case HYDRATE:
      return action.PropTypes.any
    default: {
      const combineReducer = combineReducers({
        common,
      })
      return combineReducer(state, action)
    }
  }
}

const store = configureStore<IRootReducerState>({
  reducer: rootReducer,
  devTools: !isProduction,
})
const setupStore = (context: any): EnhancedStore => store

const makeStore: MakeStore<Store> = (context) => setupStore(context)

export const wrapper = createWrapper(makeStore, { debug: false })

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
export const useReduxDispatch = () => useDispatch<AppDispatch>()
export default wrapper
