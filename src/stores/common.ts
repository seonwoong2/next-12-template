import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { ICommonRedux } from 'stores/types/common'

const initialState: ICommonRedux = {
  userInfo: null,
}

export const common = createSlice({
  name: 'common',
  initialState,
  reducers: {
    setUserInfoAction: (state: ICommonRedux, action: PayloadAction) => {
      state.userInfo = action.payload
    },
    setInitState: (state: ICommonRedux, action: PayloadAction) => {
      state = initialState
    },
  },
})

export const { setUserInfoAction, setInitState } = common.actions

export default common.reducer
