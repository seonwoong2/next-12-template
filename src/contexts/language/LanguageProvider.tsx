import { useRouter } from 'next/router'
import React from 'react'
import { IntlProvider } from 'react-intl'
import { ECookie } from 'types/util'
import { getStorageData, setStorageData } from 'utils/common'

import { isLocale } from './util'

export const LanguageContext = React.createContext({} as any)

interface Props {
  children: any
  messages: any
  lang: string
}

const isReload = (router) => {
  if (router.pathname === '/_error') return false
  return true
}

const LanguageProvider = ({ children, messages, lang }: Props) => {
  const router = useRouter()
  let _lang = lang === null || lang === 'null' || !lang ? 'en' : lang
  const [locale, setLocale] = React.useState(_lang)
  React.useEffect(() => {
    const localSetting = getStorageData(ECookie.LANGUAGE)
    if (localSetting && isLocale(localSetting)) {
      document.documentElement.lang = localSetting
      setLocale(localSetting)
    }
  }, [locale])

  return (
    <LanguageContext.Provider value={{ locale }}>
      <IntlProvider locale={locale} messages={messages[locale]}>
        {children}
      </IntlProvider>
    </LanguageContext.Provider>
  )
}
export default LanguageProvider
