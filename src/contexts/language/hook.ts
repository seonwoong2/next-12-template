import React from 'react'

import { LanguageContext } from './LanguageProvider'

export const useLocale = () => React.useContext(LanguageContext)
