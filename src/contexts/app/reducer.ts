import { IErrorCode } from 'constants/common'

export const initialState = {
  error: {
    isError: false,
    code: -1,
    message: '',
  },
}

type ActionType = { type: 'ERROR'; payload: IErrorCode } | { type: 'RESET' }

type StateType = typeof initialState

export const appReducer = (state: StateType, action: ActionType): StateType => {
  switch (action.type) {
    case 'ERROR':
      return {
        ...state,
        error: {
          ...state.error,
          isError: true,
          code: action.payload.code,
          message: action.payload.message,
        },
      }
    case 'RESET':
      return {
        ...initialState,
      }
    default: {
      console.error(`nsupported action type at App Reducer :)`)
    }
  }
}
