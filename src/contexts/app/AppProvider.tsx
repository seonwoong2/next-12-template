import { createCustomContext } from 'contexts/createContext'
import React from 'react'
import { IDefaultProps } from 'constants/common'

import { appReducer, initialState } from './reducer'

const [state, dispatch, Provider] = createCustomContext(appReducer, initialState)

interface IProps extends IDefaultProps {
  children: any
  params: any
}

const AppProvider: React.FC<IProps> = ({ children, params }) => {
  return <Provider>{children}</Provider>
}

export default AppProvider

export { dispatch as useAppDispatch }
export { state as useAppState }
