export enum SERVER {
  id = 'error.server',
  code = 500,
  defaultMessage = 'unknown server error',
  buttonType = 'confirm',
}
export enum NETWORK_ERROR {
  id = 'error.network',
  code = 600,
  defaultMessage = 'network error',
  buttonType = 'confirm',
}

export enum NO_ERROR {
  id = 'error.no_error',
  code = 20000,
  defaultMessage = 'no error',
  buttonType = 'confirm',
}

export enum NOT_FOUND_USER {
  id = 'error.not_found_user',
  code = 20001,
  defaultMessage = 'not found user',
  buttonType = 'confirm',
}

export enum REQUEST_LIMIT_EXCEEDED {
  id = 'error.request_limit_exceeded',
  code = 20012,
  defaultMessage = 'The request limit exceeded. Please try again later ',
  buttonType = 'confirm',
}

export enum REQUEST_INTERVAL_TOO_SHORT {
  id = 'error.request_interval_too_short',
  code = 20013,
  defaultMessage = 'The request interval is to short',
  buttonType = 'confirm',
}
export enum LOGIN_FAIL_DG {
  id = `error.login_fail`,
  code = 200321,
  defaultMessage = 'Login failed',
  buttonType = 'confirm',
}

export const ErrorCode = {
  20000: NO_ERROR,
  20001: NOT_FOUND_USER,
  20012: REQUEST_LIMIT_EXCEEDED,
  20013: REQUEST_INTERVAL_TOO_SHORT,
  200321: LOGIN_FAIL_DG,
}

export default ErrorCode
