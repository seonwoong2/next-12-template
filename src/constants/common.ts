export const isLocal = process.env.SERVER_LOCATION === 'local'
export const isBrowser = typeof window !== 'undefined'
export const isProduction = process.env.SERVER_LOCATION === 'production'

// MOBILE MAX
export const M_MAX = 580
// TABLET MIN
export const T_MIN = 581
// TABLET MAX
export const T_MAX = 991
// DESKTOP MIN
export const D_MIN = 992

export const APP_NAME = 'DABI TRAVEL'

export interface IDefaultProps {
  deviceType?: {
    mobile: boolean
    tablet: boolean
    desktop: boolean
  }
}

export interface IErrorCode {
  code: number
  message: string
}
