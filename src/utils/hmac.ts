import Base64 from 'crypto-js/enc-base64'
import hmacSHA256 from 'crypto-js/hmac-sha256'

export const makeHMACSignature = (method, url, timestamp) => {
  let apiAccessKey = process.env.API_ACCESS_KEY
  let apiSecretKey = process.env.API_SECRET_KEY
  let message = method + ' ' + url + '\n' + timestamp + '\n' + apiAccessKey
  let hash = hmacSHA256(message, apiSecretKey)
  let signing_key = Base64.stringify(hash)
  return signing_key
}
