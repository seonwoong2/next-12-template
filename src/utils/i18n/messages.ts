import localEn from './en.json'
import localJa from './ja.json'
import localeKo from './ko.json'
import localCn from './zh.json'

export const messages = {
  ko: localeKo,
  en: localEn,
  zh: localCn,
  ja: localJa,
}
