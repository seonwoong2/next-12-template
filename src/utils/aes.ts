import CryptoJS from 'crypto-js'

const iv = '0103050709020406'
const AES_32B_KEY = 'NYwRnTYtqvOcTAtTuH1u'

export const encrypt = (value: any) => {
  return CryptoJS.AES.encrypt(value, CryptoJS.enc.Utf8.parse(AES_32B_KEY), {
    iv: CryptoJS.enc.Utf8.parse(iv),
    padding: CryptoJS.pad.Pkcs7,
  }).toString()
}

export const decrypt = (value: any) => {
  let bytes = CryptoJS.AES.decrypt(value, CryptoJS.enc.Utf8.parse(AES_32B_KEY), {
    iv: CryptoJS.enc.Utf8.parse(iv),
    padding: CryptoJS.pad.Pkcs7,
  })

  return bytes.toString(CryptoJS.enc.Utf8)
}
