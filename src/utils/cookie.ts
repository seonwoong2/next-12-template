import Cookies from 'js-cookie'
import { ECookie } from 'types/util'
import { decrypt, encrypt } from './aes'

export const getCookie = (key: ECookie) => {
  try {
    const seriallizedState = Cookies.get(key)

    if (!seriallizedState) return seriallizedState

    let decrypted = decrypt(seriallizedState)

    return JSON.parse(decrypted)
  } catch (err) {
    console.error('err', err)
    return undefined
  }
}

export const setCookie = (key: ECookie, value: any, expires: any) => {
  try {
    const seriallizedState = JSON.stringify(value)
    let encrypted = encrypt(seriallizedState)
    Cookies.set(key, encrypted, { ...expires, sameSite: 'None', secure: true })
  } catch (err) {
    console.error('err', err)
  }
}

export const getServerSideCookie = (cookiename: any, cookiestring: any) => {
  let name = cookiename + '='
  let decodedCookie = decodeURIComponent(cookiestring)
  let ca = decodedCookie.split(';')
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i]
    while (c.charAt(0) === ' ') {
      c = c.substring(1)
    }
    if (c.indexOf(name) === 0) {
      try {
        let decrypted = decrypt(c.substring(name.length, c.length))
        return JSON.parse(decrypted)
      } catch (err) {
        return undefined
      }
    }
  }
  return undefined
}

export const removeCookie = (key: ECookie) => {
  Cookies.remove(key)
}
