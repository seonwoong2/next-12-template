import { isBrowser } from 'constants/common'
import { ECustomEvent, IConfirmAlertArgs } from 'types/util'

export const showErrorPopup = ({ buttonType, title, message, needTranslate, onCancel, onClose, onConfirm, forcePopup, ...props }: IConfirmAlertArgs) => {
  if (isBrowser) {
    window.dispatchEvent(
      new CustomEvent(ECustomEvent.ERROR_POPUP, {
        detail: {
          title,
          message,
          needTranslate,
          forcePopup,
          buttonType,
          onConfirm,
          onClose,
          onCancel,
          ...props,
        },
      }),
    )
  }
}
