import { ECookie } from 'types/util'
import { isBrowser, isLocal } from 'constants/common'

import { getSessionState, removeSessionState, setSessionState } from './session'
import { getCookie, removeCookie, setCookie } from './cookie'
import { ELanguage } from 'types/common'

export const getLanguageByQueryParam = (p) => {
  if (isBrowser) return getStorageData(ECookie.LANGUAGE) || ELanguage.EN
  return ELanguage.EN
}

export const getToken = () => {
  if (isBrowser) return getStorageData(ECookie.ACCESS_TOKEN)
  return undefined
}

export const getStorageData = (key: ECookie) => {
  if (isLocal) {
    return getSessionState(key)
  }
  return getCookie(key)
}
export const setStorageData = (key: ECookie, value, expires = undefined) => {
  if (isLocal) {
    return setSessionState(key, value)
  }
  return setCookie(key, value, expires)
}

export const removeStorageData = (key: ECookie) => {
  if (isLocal) {
    return removeSessionState(key)
  }
  return removeCookie(key)
}
