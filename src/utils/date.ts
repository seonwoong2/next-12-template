type formatTimeType = 'all' | 'time' | 'date' | 'noYear' | 'noSeconds' | 'dateTime' | 'restExcludedYear' | 'shortYear'

export const formatTime = (time: Date, type: formatTimeType = 'all', isoffset: boolean = true): string => {
  const date = new Date()
  const offset = isoffset ? 0 - date.getTimezoneOffset() : 0
  let c_time = new Date(time)
  if (isNaN(c_time.getTime())) {
    return undefined
  }
  const temp_time = c_time.getTime() + offset * 60 * 1000
  c_time = new Date(temp_time)
  let Datestr = ''
  switch (type) {
    case 'time':
      Datestr = c_time.getHours() + ':' + c_time.getMinutes() + ':' + c_time.getSeconds()
      break
    case 'date':
      Datestr = c_time.getFullYear() + '-' + (c_time.getMonth() + 1) + '-' + c_time.getDate()
      break
    case 'dateTime':
      Datestr = c_time.getMonth() + 1 + '-' + c_time.getDate() + ' ' + c_time.getHours() + ':' + c_time.getMinutes()
      break
    case 'noYear':
      Datestr = c_time.getMonth() + 1 + '-' + c_time.getDate()
      break
    case 'noSeconds':
      Datestr = c_time.getFullYear() + '-' + (c_time.getMonth() + 1) + '-' + c_time.getDate() + ' ' + c_time.getHours() + ':' + c_time.getMinutes()
      break
    case 'restExcludedYear':
      Datestr = c_time.getMonth() + 1 + '-' + c_time.getDate() + ' ' + c_time.getHours() + ':' + c_time.getMinutes() + ':' + c_time.getSeconds()
      break
    case 'shortYear':
      Datestr = c_time.getFullYear().toString().substring(2, 4) + '-' + (c_time.getMonth() + 1) + '-' + c_time.getDate()
      break
    default:
      Datestr =
        c_time.getFullYear() +
        '-' +
        (c_time.getMonth() + 1) +
        '-' +
        c_time.getDate() +
        ' ' +
        c_time.getHours() +
        ':' +
        c_time.getMinutes() +
        ':' +
        c_time.getSeconds()
      break
  }

  return Datestr.replace(/(\b|\:|-|\s)(\d)(?=\D|$)/g, '$10$2')
}
