import BigNumber from 'bignumber.js'

export const accAdd = (a, b) => {
  const bignumber = new BigNumber(a)
  return bignumber.plus(b).toString()
}

export const accSub = (a, b) => {
  const bignumber = new BigNumber(a)
  return bignumber.minus(b).toString()
}

export const accDiv = (a, b) => {
  const bignumber = new BigNumber(a)
  return bignumber.dividedBy(b).toString()
}

export const accMul = (a, b) => {
  const bignumber = new BigNumber(a)
  return bignumber.multipliedBy(b).toString()
}
