import ErrorCode from 'constants/error'
import { AlertButtonType } from 'types/util'

export const isError = (errorCode: number) => {
  return errorCode !== 20000
}

export const getErrorData = (code) => {
  let data: { id: string; code: number; defaultMessage: string; buttonType: AlertButtonType } = ErrorCode[code]
  if (!data) {
    data = ErrorCode[500]
  }
  return data
}
