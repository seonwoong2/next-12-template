import BigNumber from 'bignumber.js'

export const strip = (value) => {
  value = value.toString()
  if (value.indexOf('.') === -1) {
    return value
  }
  let from = value.length - 1

  do {
    if (value[from] === '0') {
      from--
    }
  } while (value[from] === '0')

  if (value[from] === '.') {
    from--
  }
  return value.substr(0, from + 1)
}

export const toMoney = (num, precision = 0, useStrip = true) => {
  let number = typeof num === 'string' ? num.replaceAll(',', '') : num
  let bn = new BigNumber(number)
  let result = useStrip ? strip(bn.toFormat(precision)) : bn.toFormat(precision)
  return result === 'NaN' ? '' : result
}

export const toMoneyRoundDown = (num, precision = 0, useStrip = true) => {
  let number = typeof num === 'string' ? num.replaceAll(',', '') : num
  let bn = new BigNumber(number)
  const toFormat = bn.toFormat(precision, BigNumber.ROUND_DOWN)
  let result = useStrip ? strip(toFormat) : toFormat
  return result === 'NaN' ? '' : result
}

export const toMoneyRoundUp = (num, precision = 0, useStrip = true) => {
  let number = typeof num === 'string' ? num.replaceAll(',', '') : num
  let bn = new BigNumber(number)
  const toFormat = bn.toFormat(precision, BigNumber.ROUND_UP)
  return useStrip ? strip(toFormat) : toFormat
}
