import { isBrowser } from 'constants/common'
import { ECookie } from 'types/util'
import { decrypt, encrypt } from './aes'

export const getSessionState = (key: any) => {
  try {
    const seriallizedState = sessionStorage.getItem(key)
    if (sessionStorage === null) {
      return undefined
    }

    let decrypted = decrypt(seriallizedState)

    return JSON.parse(decrypted)
  } catch (err) {
    return undefined
  }
}

export const setSessionState = (key: any, value: any) => {
  try {
    const seriallizedState = JSON.stringify(value)
    let encrypted = encrypt(seriallizedState)
    sessionStorage.setItem(key, encrypted)
  } catch (err) {}
}

export const getUserInfo = () => {
  if (isBrowser) {
    return getSessionState('user')
  }
  return undefined
}

export const removeSessionState = (key: any) => {
  try {
    sessionStorage.removeItem(key)
  } catch (err) {}
}
