export enum ERouterPath {
  MAIN = '/',
  LOGIN = '/login',
  REGISTER = 'register',
}
