export enum ECookie {
  ACCESS_TOKEN = 'DG_A',
  LANGUAGE = 'DG_L',
  HOST = 'HOST',
}

export type AlertButtonType = 'default' | 'confirm' | 'cancel' | 'close' | 'home' | 'custom' | 'signout' | 'reload' | 'move'

export enum ECustomEvent {
  ERROR_POPUP = 'ERROR_POPUP',
}

export interface IConfirmAlertArgs {
  code?: number
  status?: number
  title?: string
  message: string
  buttonType: AlertButtonType
  needTranslate?: boolean
  forcePopup?: boolean
  style?: any
  onConfirm?: () => void
  onClose?: () => void
  onCancel?: () => void
}
