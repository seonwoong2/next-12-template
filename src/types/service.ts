export interface IErrors {
  message: string
  code?: number
  status?: number
}
