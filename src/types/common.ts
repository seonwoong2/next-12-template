export const LANGUAGES = [
  {
    id: 'ko',
    defaultMessage: '한국어',
    text: 'ko',
  },
  {
    id: 'en',
    defaultMessage: 'English',
    text: 'en',
  },
  {
    id: 'ja',
    defaultMessage: '日本語',
    text: 'ja',
  },
  {
    id: 'zh',
    defaultMessage: '中國語',
    text: 'zh',
  },
]

export enum ELanguage {
  KO = 'ko',
  EN = 'en',
  JA = 'ja',
  ZH = 'zh',
}

export interface ICountryType {
  code: string
  label: string
  phone: string
  suggested?: boolean
}
