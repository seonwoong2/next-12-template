import { NextPage } from 'next'
import { IDefaultProps } from 'constants/common'
import { gql, useMutation } from '@apollo/client'

import React from 'react'

const Login: NextPage<IDefaultProps> = () => {
  const [testLogin, { data, loading, error }] = useMutation(LOGIN, {
    variables: {
      input: {
        email: 'lsosub5936@gmail.com',
        password: 'tjsdnd963!',
      },
    },
  })
  return (
    <div>
      <button onClick={() => testLogin()}>test</button>
    </div>
  )
}

export default Login

const LOGIN = gql`
  mutation gamebet($input: LoginInput!) {
    login(input: $input) {
      access_token
      refresh_token
      member {
        id
        name
        email
        mobile
        language
        local_mobile
        country_code
      }
      error {
        code
        message
      }
    }
  }
`
