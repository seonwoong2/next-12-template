import React from 'react'
import { NextPage } from 'next'
import { IDefaultProps } from 'constants/common'
import { MainTemplate } from 'components/page/main/template'
import { BasicLayout } from 'layouts/Layout/BasicLayout'
import { BasicHeader } from 'layouts/Header/BasicHeader'

const Home: NextPage<IDefaultProps> = ({ deviceType }) => {
  return (
    <BasicLayout deviceType={deviceType} header={<BasicHeader />} useFooter={false}>
      <MainTemplate deviceType={deviceType} />
    </BasicLayout>
  )
}

export default Home
