import 'styles/globals.css'
import { useGetDeviceType } from 'hooks/useDeviceType'
import React, { Suspense } from 'react'
import { APP_NAME } from 'constants/common'
import { ApolloProvider } from '@apollo/client'
import { useApollo } from 'services/apollo/hook'
import type { AppContext, AppProps } from 'next/app'
import { NextPageContext } from 'next'
import { LanguageProvider } from 'contexts/language'
import wrapper from 'stores/configure'
import { Provider as ReduxProvider } from 'react-redux'
import App from 'next/app'
import { getLanguageByQueryParam, getToken } from 'utils/common'
import CustomAlert from 'components/molecule/CustomAlert/CustomAlert'
import { theme } from 'styles/theme'
import { ThemeProvider } from 'styled-components'
import { messages } from 'utils/i18n/messages'
import AppProvider from 'contexts/app/AppProvider'

interface MyAppProps extends AppProps {
  pageProps: any
  header: any
}

export const ExtendedApp = (res: MyAppProps) => {
  const { store, props } = wrapper.useWrappedStore(res)
  const { Component, pageProps, header, router } = props
  const queryParam = router.query
  const deviceType = useGetDeviceType()
  const lang = getLanguageByQueryParam(queryParam)
  const token = getToken()
  const apolloClient = useApollo(pageProps.initialApolloState, token, lang, props.header)

  return (
    <React.Fragment>
      <LanguageProvider messages={messages} lang={messages[lang as string] ? (lang as string) : 'en'}>
        <ReduxProvider store={store}>
          <ThemeProvider theme={theme}>
            <ApolloProvider client={apolloClient}>
              <AppProvider deviceType={deviceType} params={queryParam}>
                <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
                <title>{APP_NAME}</title>
                <Component deviceType={deviceType} {...pageProps} />
                <CustomAlert />
              </AppProvider>
            </ApolloProvider>
          </ThemeProvider>
        </ReduxProvider>
      </LanguageProvider>
    </React.Fragment>
  )
}

ExtendedApp.getInitialProps = async (appContext: AppContext) => {
  const ctx: NextPageContext = appContext.ctx
  const appProps = await App.getInitialProps(appContext)
  return { ...appProps, header: ctx?.req?.headers, err: ctx.err }
}

export default ExtendedApp
