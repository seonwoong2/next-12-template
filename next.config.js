/** @type {import('next').NextConfig} */

const initEnv = {
  HOST: process.env.HOST,
  API_ACCESS_KEY: process.env.API_ACCESS_KEY,
  API_SECRET_KEY: process.env.API_SECRET_KEY,
  SERVER_LOCATION: process.env.SERVER_LOCATION,
  REST_API_HOST: process.env.REST_API_HOST,
  REST_API_BASE_URL: process.env.REST_API_BASE_URL,
}

const nextConfig = {
  compiler: {
    styledComponents: true,
    removeConsole: process.env.NODE_ENV === 'production' ? true : false,
  },
  productionBrowserSourceMaps: false,
  reactStrictMode: false,
  swcMinify: true,
  env: initEnv,
  typescript: {
    ignoreBuildErrors: false,
  },
  async rewrites() {
    return [
      {
        source: '/dabi-games',
        destination: `${process.env.GRAPHQL_API}`,
      },
    ]
  },
}

module.exports = nextConfig
